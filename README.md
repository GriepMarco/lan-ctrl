# LAN Control (Deutsch/German) (Scroll down for English)

## Wofür ist das?
LAN Control (LAN Ctrl) ist ein Windows Dienst der die deaktivierung von Netzwerkadapter erzwingt. Ob Netzwerkadapter aktiv sind, wird alle 10 Sekunden geprüft und gegebenenfalls wieder deaktiviert. Die Anwendung ist für Situationen gedacht, bei dem eine Netzwerkverbindung von Geräten nicht erwünscht ist. z.B. für Wahlen. LAN Ctrl ist eine Alternative möglichkeit zur Deaktivierung von Geräten im BIOS.

## Lizenz (MIT License)
Copyright 2024 Marco Griep

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Quellcode
Den aktuellen Quellcode findet Ihr im Unterordner "Source". Es handelt sich um ein C# Windows Dienst.

## Aktuelles Release
Das aktuelle Release befindet sich unter PSAppDeployToolkit/Files/LanCtrl.exe

## Installationsmethoden

### Installation über Chocolatey
Ihr könnt den LANCtrl Dienst einfach über Chocolatey installieren. (Vorausgesetzt Ihr habt [Chocolatey](https://chocolatey.org/install) installiert).
Gebt dazu einfach folgenden Befehl in Eurer Commandozeile ein:

```
choco install lan-ctrl -s="https://nuget.marcogriep.de/v3/index.json"
```

**INFO:** *Der Dienst wird nur installiert und steht auf automatisch starten, wird nach der Installation jedoch nicht automatisch gestartet, sondern erst beim nächsten Systemstart. Starten Sie den Dienst bei Bedarf nach der Installation manuell oder Rebooten Sie Ihren Computer.*

**Über Chocolatey updaten**

```
choco upgrade lan-ctrl -s="https://nuget.marcogriep.de/v3/index.json"
```

### Installation über PSAppDeployToolkit
Das PSAppDeployToolkit ermöglicht die einfache und unbeaufsichtige Installation von Software über Softwareverteilungslösungen wie NinjaOne, SCCM, Ivanti DSM, OPSI. Sie können mit PSAppDeployToolkit aber auch einfache Installationsroutinen automatisieren. Starten Sie einfach im Ordner PSAppDeployToolkit die "Deploy-Application.exe" als Administrator. Sie bekommen eine Notification sobald die Installation abgeschlossen ist.

**INFO:** *Der Dienst wird nur installiert und steht auf automatisch starten, wird nach der Installation jedoch nicht automatisch gestartet, sondern erst beim nächsten Systemstart. Starten Sie den Dienst bei Bedarf nach der Installation manuell oder Rebooten Sie Ihren Computer.*

### Manuelle Installation
Laden Sie sich das Repository aus GitLab herunter und kopieren Sie die Dateien aus dem Ordner PSAppDeployToolkit/Files/ in den Ordner C:\LANCtrl
Führen Sie in der cmd (Nicht Powershell) folgenden Befehl mit Adminrechten aus um den Dienst in Windows zu registrieren.

```
sc create LANCtrl binpath= C:\LANCtrl\LANCtrl.exe start= auto
```

**INFO:** *Der Dienst wird nur installiert und steht auf automatisch starten, wird nach der Installation jedoch nicht automatisch gestartet, sondern erst beim nächsten Systemstart. Starten Sie den Dienst bei Bedarf nach der Installation manuell oder Rebooten Sie Ihren Computer.*


## Deinstallation
Es gibt aktuell noch keine offizielle Deinstallationsroutine. Um den Dienst wieder loszuwerden, öffnen Sie als Administrator eine cmd und führen Sie folgenden
Befehlt aus:

```
sc stop LANCtrl
sc delete LANCtrl
```

Anschließend löschen Sie das Verzeichnis C:\LANCtrl


## Mitarbeit
Gerne können Sie Änderungen hier Commiten oder das Projekt Forken.

## Sag danke
Wenn das Tool hilfreich war, würde ich mich über eine nette Mail freuen: mail@marcogriep.de
Schaut gern mal auf meiner Website vorbei: www.marcogriep.de, vielleicht die ist ein oder andere Dienstleistung interessant.

---

# LAN Control (English)

## What is this for?
LAN Control (LAN Ctrl) is a Windows service that forces the deactivation of network adapters. Whether network adapters are active is checked every 10 seconds and deactivated if necessary. The application is intended for situations where a network connection from devices is not desired, for example, during elections. LAN Ctrl is an alternative method for disabling devices in the BIOS.

## License (MIT License)
Copyright 2024 Marco Griep

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Source Code
You can find the current source code in the "Source" subfolder. It is a C# Windows service.

## Current Release
The current release is located at PSAppDeployToolkit/Files/LanCtrl.exe

## Installation Methods

### Installation via Chocolatey
You can easily install the LANCtrl service using Chocolatey. (Assuming you have [Chocolatey](https://chocolatey.org/install) installed).
Simply enter the following command into your command line:

```
choco install lan-ctrl -s="https://nuget.marcogriep.de/v3/index.json"
```

**INFO:** *The service is only installed and set to start automatically, but it will not start automatically after installation; it will start on the next system restart. Start the service manually after installation if needed or reboot your computer.*

**Updating via Chocolatey**

```
choco upgrade lan-ctrl -s="https://nuget.marcogriep.de/v3/index.json"
```

### Installation via PSAppDeployToolkit
The PSAppDeployToolkit allows for easy and unattended software installation via software distribution solutions like NinjaOne, SCCM, Ivanti DSM, OPSI. However, you can also use PSAppDeployToolkit to automate simple installation routines. Simply run "Deploy-Application.exe" as an administrator in the PSAppDeployToolkit folder. You will receive a notification once the installation is completed.

**INFO:** *The service is only installed and set to start automatically, but it will not start automatically after installation; it will start on the next system restart. Start the service manually after installation if needed or reboot your computer.*

### Manual Installation
Download the repository from GitLab and copy the files from the PSAppDeployToolkit/Files/ folder to the C:\LANCtrl folder. In the cmd (Not Powershell), execute the following command with admin rights to register the service in Windows.

```
sc create LANCtrl binpath= C:\LANCtrl\LANCtrl.exe start= auto
```

**INFO:** *The service is only installed and set to start automatically, but it will not start automatically after installation; it will start on the next system restart. Start the service manually after installation if needed or reboot your computer.*


## Uninstallation
There is currently no official uninstallation routine. To remove the service, open a cmd as an administrator and execute the following command:

```
sc stop LANCtrl
sc delete LANCtrl
```

Then delete the directory C:\LANCtrl.

## Contribution
Feel free to Commit Changes or fork the Project

## Say thanks
If the tool was helpful, I would appreciate a kind email at: mail@marcogriep.de
Feel free to visit my website: www.marcogriep.de, perhaps some of the services there are of interest.