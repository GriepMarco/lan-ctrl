robocopy "PSAppDeployToolkit" "Chocolatey\tools" /MIR
copy "Chocolatey\chocolateybeforemodify.ps1" "Chocolatey\tools\chocolateybeforemodify.ps1"
copy "Chocolatey\chocolateyinstall.ps1" "Chocolatey\tools\chocolateyinstall.ps1"
copy "Chocolatey\chocolateyuninstall.ps1" "Chocolatey\tools\chocolateyuninstall.ps1"
choco pack Chocolatey/lan-ctrl.nuspec --outputdirectory build
PAUSE