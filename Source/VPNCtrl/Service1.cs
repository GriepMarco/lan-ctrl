﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LANCtrl
{

    public partial class Service1 : ServiceBase
    {
        private readonly Timer timer;
        private StreamWriter logWriter;

        public Service1()
        {
            InitializeComponent();
            EnsureLogDirectoryExists();
            timer = new Timer();
            timer.Interval = 10000; // Überprüfen alle 5 Sekunde
            timer.Elapsed += OnTimerElapsed;
        }

        protected override void OnStart(string[] args)
        {
            timer.Start();
            Log($"Service LANCtrl started.");
            base.OnStart(args);
        }

        protected override void OnStop()
        {
            timer.Stop();
            Log($"Service LANCtrl stopped.");
            logWriter.Close();
            base.OnStop();
        }

        private async void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                var networkInterfaces = NetworkAdapterUtil.GetNetworkInterfaces();

                foreach (var eth in networkInterfaces)
                {
                    if (NetworkAdapterUtil.IsAdpapterEnabled(eth.Name))
                        await NetworkAdapterUtil.DisableAdapterAsync(eth.Name, 5000);
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message);
            }

        }

        private void EnsureLogDirectoryExists()
        {
            string logDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "LANCtrl Logs");
            if (!Directory.Exists(logDirectory))
            {
                Directory.CreateDirectory(logDirectory);
            }
            string logFilePath = Path.Combine(logDirectory, "trace.log");
            logWriter = new StreamWriter(logFilePath, true);
        }

        private void Log(string message)
        {
            string logMessage = $"{DateTime.Now} - {message}";
            Console.WriteLine(logMessage);
            logWriter.WriteLine(logMessage);
            logWriter.Flush();
        }

        private void LogError(string errorMessage)
        {
            string logErrorMessage = $"{DateTime.Now} - ERROR: {errorMessage}";
            Console.Error.WriteLine(logErrorMessage);
            logWriter.WriteLine(logErrorMessage);
            logWriter.Flush();
        }
    }
}

